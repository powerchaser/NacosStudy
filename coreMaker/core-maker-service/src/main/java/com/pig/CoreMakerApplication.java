package com.pig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @description: 启动类
 * @author: wupeng
 * @create: 2020-04-13 17:22
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class CoreMakerApplication {
    public static void main(String[] args) {
        SpringApplication.run(CoreMakerApplication.class, args);
    }
}
