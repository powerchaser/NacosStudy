package com.pig.nacos.service;

import com.pig.nacos.api.CoreMakerApi;
import org.apache.dubbo.config.annotation.Service;

/**
 * @description:
 * @author: wupeng
 * @create: 2020-04-16 17:48
 **/
@Service
public class CoreMakerImpl implements CoreMakerApi {
    public String getCore() {
        return "成功调用CoreMakerImpl";
    }
}
