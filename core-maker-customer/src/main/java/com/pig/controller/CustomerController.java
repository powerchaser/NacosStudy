package com.pig.controller;

import cn.hutool.core.lang.Console;
import com.pig.feign.CoreMakerCustomerClient;
import com.pig.nacos.service.MallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 服务提供接口
 * @author: wupeng
 * @create: 2020-04-13 17:02
 **/
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CoreMakerCustomerClient coreMakerCustomerClient;


    @GetMapping("/getCore")
    public String showCore() {
        Console.error("成功访问customer");
        return coreMakerCustomerClient.showCore();
    }
}
