package com.pig.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "coreMaker")
public interface CoreMakerCustomerClient {

    @GetMapping("/maker/core")
    String showCore();

}
