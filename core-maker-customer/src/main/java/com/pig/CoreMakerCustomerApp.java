package com.pig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @description: 启动类
 * @author: wupeng
 * @create: 2020-04-13 17:22
 **/
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class CoreMakerCustomerApp {
    public static void main(String[] args) {
        SpringApplication.run(CoreMakerCustomerApp.class, args);
    }
}
