package com.pig.nacos.controller;

import cn.hutool.core.lang.Console;
import com.pig.nacos.service.MallService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 服务提供接口
 * @author: wupeng
 * @create: 2020-04-13 17:02
 **/
@RestController
@RequestMapping("/mall")
public class AppController {

    @Reference
    private MallService mallService;

    @GetMapping("/mallInfo")
    public String getMallInfo() {
        return "通过App | " + mallService.mallPage();
    }

}
