package com.pig.nacos.impl;

import com.pig.nacos.api.CoreMakerApi;
import com.pig.nacos.service.MallService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;

/**
 * @description:
 * @author: wupeng
 * @create: 2020-04-16 11:43
 **/
@Service
public class MallServiceImpl implements MallService {

    @Reference
    CoreMakerApi coreMakerApi;

    public String mallPage() {
        return "成功访问MallService | " + coreMakerApi.getCore();
    }
}
