package com.pig.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @description:
 * @author: wupeng
 * @create: 2020-04-16 15:22
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class MallServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(MallServiceApp.class, args);
    }
}
