# NacosStudy

#### 介绍
简单的微服务架构
Nacos 配置、注册中心
Feign 消费服务调用
Dubbo RPC远程服务调用
Zuul 网关

#### 软件架构
外部调用网关gateway（zuul），转发到具体服务，服务之间dubbo调用
用nacos管理配置和服务注册


#### 安装教程

启动nacos添加配置 Data ID:api-gateway.yaml Group:XIAOWEI_MICROSERVICE_GROUP
zuul:
  routes:
    app-server:
      stripPrefix: false
      path: /app-server/**
      
依次启动
CoreMakerCustomerApp.class 
MallServiceApp.class 
AppApplication.class 
ApiGatewayApplication.class 

然后访问http://127.0.0.1:8083/app-server/mall/mallInfo
得到：通过App | 成功访问MallService | 成功调用CoreMakerImpl


core-maker-customer模块是测试feign访问方式的，无具体含义